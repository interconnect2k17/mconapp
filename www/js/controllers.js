angular.module('starter.controllers', [])
  .run(function($rootScope) {
    $rootScope.conferenceData = {};
    $rootScope.conference_name="";
  })
  .controller('AppCtrl', function($scope, $ionicModal, $timeout) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
      $scope.modal.show();
    };

    // Perform the login action when the user submits the login form
    $scope.doLogin = function() {
      console.log('Doing login', $scope.loginData);

      // Simulate a login delay. Remove this and replace with your login
      // code if using a login system
      $timeout(function() {
        $scope.closeLogin();
      }, 1000);
    };
  })

  .controller('PlaylistsCtrl', function($scope) {
    $scope.playlists = [{
        title: 'Reggae',
        id: 1
      },
      {
        title: 'Chill',
        id: 2
      },
      {
        title: 'Dubstep',
        id: 3
      },
      {
        title: 'Indie',
        id: 4
      },
      {
        title: 'Rap',
        id: 5
      },
      {
        title: 'Cowbell',
        id: 6
      }
    ];
  })
  .controller('MainCtrl', function($scope) {

  })
  .controller('ActiveConferencesCtrl', function($http, $scope, $rootScope) {
    $http.get("http://localhost:6001/getActiveConferences").then(function(response) {
      alert(JSON.stringify(response.data));
      $scope.active_conflist = response.data;
    })
    $scope.assign=function(data){
      alert(data);
      $rootScope.conference_name=data;
    }
  })


  .controller('conferenceDashboardCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {
    
    $http.get("http://saisasank001.cloudant.com/mcon_conferences/IBM%20Interconnect").then(function(response) {
      alert(JSON.stringify(response.data));

      var demo = response.data;
      $rootScope.conferenceData = demo;
      $scope.agenda = $rootScope.conferenceData.agenda;
      $scope.speakers = $rootScope.conferenceData.speakers;
    })
  })

  .controller('addConferenceCtrl', function($scope, $rootScope, $http, $state) {
    $scope.params = {};
    $scope.addConferenceDetails = function(params) {

      var conference_Name = params.conferenceName;
      var conference_Location = params.conference_Location;
      var conference_FromDate = params.conference_FromDate;
      var conference_ToDate = params.conference_ToDate;
      var conference_Description = params.conference_Description;

      $scope.conferenceDetails = {
        "_id": conference_Name,
        "conference_name": conference_Name,
        "conference_location": conference_Location,
        "cconference_fromDate": conference_FromDate,
        "conference_toDate": conference_ToDate,
        "conference_description": conference_Description,

      };

      $rootScope.conferenceData.details = $scope.conferenceDetails;
      alert(JSON.stringify($rootScope.conferenceData));
    };

    $scope.addConferenceAgenda = function(params) {

      alert(JSON.stringify($rootScope.conferenceData));
      $rootScope.conferenceData.agenda = params.conferenceAgenda;
      alert(JSON.stringify($rootScope.conferenceData));
    };

    $scope.addConferenceSpeakers = function(params) {
      alert(JSON.stringify($rootScope.conferenceData));
      $rootScope.conferenceData.speakers = params;
      alert(JSON.stringify($rootScope.conferenceData));
    };

    $http.get("https://ibmcloudant01.cloudant.com/employee/_all_docs?include_docs=true").then(function(response) {
      alert(JSON.stringify(response.data.rows));

      $scope.listPref = response.data.rows;
    })

    $scope.checkItems = {}

    $scope.print = function() {

      console.log($scope.checkItems);
    }

    $scope.save = function() {
      var assigned_array = [];
      for (i in $scope.checkItems) {
        console.log($scope.checkItems[i]);
        if ($scope.checkItems[i] == true) {
          assigned_array.push(i);
        }
      }
      $rootScope.conferenceData.attendees = assigned_array;
      $rootScope.conferenceData.status="active";
      alert(JSON.stringify($rootScope.conferenceData));
      var link = 'http://127.0.0.1:6001/addConference';

      $http.post(link, $rootScope.conferenceData).then(function(res) {
        alert(JSON.stringify(res));
        if (res.data.success) {
          $state.go('app.activeconferences');
        } else {
          alert("something went wrong. Please try again");
        }
      });
    }
  })

  .controller('inactiveConferencesCtrl', function($http, $scope, $rootScope, $location, $ionicPopup) {

    $http.get("http://localhost:6001/getInActiveConferences").then(function(response) {
      alert(JSON.stringify(response.data));
      $scope.inactive_conflist = response.data;
    })
  })

  .controller('addMeetingdetailsCtrl', function($scope, $http, $rootScope) {
    $scope.params = {};
    $scope.addMeetingDetails = function(params) {

      console.log(JSON.stringify(params));

      var _id = params.meeting_title;
      var meeting_title = params.meeting_title;
      var meeting_purpose = params.meeting_purpose;
      var meeting_location = params.meeting_location;
      var meeting_date = params.meeting_date;
      var meeting_fromTime = params.meeting_fromTime;
      var meeting_toTime=params.meeting_toTime;
      var meeting_status = params.meeting_status;

      var meetingDetails = {
        "_id": meeting_title,
        "meeting_title": meeting_title,
        "meeting_purpose": meeting_purpose,
        "meeting_location": meeting_location,
        "meeting_date": meeting_date,
        "meeting_fromTime": meeting_fromTime,
        "meeting_toTime":meeting_toTime,
        "status": "active"
      };
      $http.post(" https://ibmcloudant01.cloudant.com/meetings", meetingDetails).then(function(response) {
        if (response.data.error == undefined) {

          console.log("Success");
          console.log(JSON.stringify(response));
        } else {

          console.log("Error:");
        }

      })

      alert(JSON.stringify(meetingDetails));

    }
  })


  .controller('addTextCtrl', function($scope, $rootScope) {
    $scope.params = {};
    $scope.inputs = [];
    $scope.addfield = function() {
      $scope.inputs.push({});
    }

    $scope.save = function(item) {
      alert($scope.item);




    }
  })

  .controller('ActivemeetingCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {
    $http.get("http://localhost:8000/meeting?index=active").then(function(response) {
      alert(JSON.stringify(response.data));

      $scope.active_meetings = response.data;
    })

  })

  .controller('InactivemeetingCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {
    $http.get("http://localhost:8000/meeting?index=inactive").then(function(response) {
      alert(JSON.stringify(response.data));

      $scope.inactive_meetings = response.data;


    })
  })

  .controller('UpcomingmeetingCtrl', function($scope, $rootScope, $location, $ionicPopup) {

    $scope.upcoming_meetings = [{
        id: 'meeting1',
        title: 'meeting1',
        location: 'USA',
        date: '1/1/2017',
        time: '10:00am-1:00pm'
      },
      {
        id: 'meeting2',
        title: 'meeting2',
        location: 'Uk',
        date: '2/2/2017',
        time: '11:00am-2:00pm'
      },
      {
        id: 'meeting3',
        title: 'meeting3',
        location: 'India',
        date: '15/3/2017',
        time: '1:00pm-3:00pm'
      },

    ];

  })

  .controller('previousmeetingCtrl', function($scope, $rootScope, $location, $ionicPopup) {

    $scope.previous_meetings = [{
        id: 'meetingA',
        title: 'meetingA',
        location: 'UK',
        date: '1/12/2016',
        time: '10:00am-1:00pm'
      },
      {
        id: 'meetingB',
        title: 'meetingB',
        location: 'USA',
        date: '2/2/2016',
        time: '11:00am-2:00pm'
      },
    ];


  })
  .controller('UserConferencesCtrl', function($http, $scope, $rootScope) {
    $http.get("http://localhost:6001/getActiveConferences").then(function(response) {
      alert(JSON.stringify(response.data));

      $scope.user_conflist = response.data;
    })
  })


  .controller('viewmeetingCtrl', function($scope, $rootScope, $location, $ionicPopup) {

    $scope.view_meeting = [{
        purpose: 'To discuss on projects',
        location: 'UK',
        date: '1/12/2016',
        time: '10:00am-1:00pm'
      },
    ];
  })
  .controller('view_sessionCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {
    $http.get("https://ibmcloudant01.cloudant.com/mcon/conference user session list").then(function(response) {
      alert(JSON.stringify(response.data));

      $scope.view_session = response.data;
    })
  })

  .controller('meetingdetailsCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {

    $http.get("https://ibmcloudant01.cloudant.com/meetings/eebfc951ae4a7e8c24997585ee82d5c9").then(function(response) {
      alert(JSON.stringify(response.data));

      $scope.meeting_details = response.data;
    })
  })

  .controller('newattendeeCtrl', function($scope, $http, $rootScope) {
    $scope.params = {};
    $scope.insertData = function(params) {
      console.log(JSON.stringify(params));


      var _id = params.newattendee_name;
      var newattendee_name = params.newattendee_name;
      var newattendee_phoneno = params.newattendee_phoneno;
      var newattendee_company = params.newattendee_company;
      var newattendee_designation = params.newattendee_designation;
      var person_name = params.person_name;
      var person_field = params.person_field;
      var person_contact = params.person_contact;


      var newattendeeDetails = {

        "_id": newattendee_name,
        "newattendee_name": newattendee_name,
        "newattendee_phoneno": newattendee_phoneno,
        "newattendee_company": newattendee_company,
        "newattendee_designation": newattendee_designation,
        "person_name": person_name,
        "person_field": person_field,
        "person_contact": person_contact

      };
      $http.post("https://ibmcloudant01.cloudant.com/attendee", newattendeeDetails).then(function(response) {
        if (response.data.error == undefined) {

          console.log("Success");
          console.log(JSON.stringify(response));
        } else {

          console.log("Error:");
        }

      })
      alert(JSON.stringify(newattendeeDetails));

    }
  })

  .controller('view_newattendeeCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {
    $http.get("https://ibmcloudant01.cloudant.com/attendee/HAri").then(function(response) {
      alert(JSON.stringify(response.data));
      $scope.view_newattendee = response.data;
      /*$scope.view_newattendee=[{attendee_name:'mike',phno:'9912576777',company:'MSS',designation:'lead researcher',name:'john',field:'Marketing',contact:'9996663332'},
      ];*/
    })
  })

  .controller('userssessionsCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {

    $http.get("http://localhost:7000/retrieve?session=harry").then(function(response) {
      alert(JSON.stringify(response.data));
      $scope.user_sessions = response.data;

    })
  })

  .controller('teamsessionsCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {

    $http.get("https://ibmcloudant01.cloudant.com/sessionnotes/_all_docs?include_docs=true").then(function(response) {
      alert(JSON.stringify(response.data));
      $scope.team_sessions = response.data;

    })
  })
  .controller('add_sessionCtrl', function($scope, $http, $rootScope) {
    $scope.params = {};
    $scope.add = function(params) {
      console.log(JSON.stringify(params));


      var _id = params.session_title;
      var session_title = params.session_title;
      var session_location = params.session_location;
      var session_speaker = params.session_speaker;
      var session_date = params.session_date;
      var session_time = params.session_time;
      var session_notes = params.session_notes;
      var newsessionDetails = {

        "_id": params.session_title,
        "session_title": session_title,
        "session_location": session_location,
        "session_speaker": session_speaker,
        "session_date": session_date,
        "session_time": session_time

      };
      $http.post("https://ibmcloudant01.cloudant.com/mcon", newsessionDetails).then(function(response) {
        if (response.data.error == undefined) {

          console.log("Success");
          console.log(JSON.stringify(response));
        } else {

          console.log("Error:");
        }

      })
      alert(JSON.stringify(newsessionDetails));

    }
  })
  .controller('view_sessionCtrl', function($scope, $http, $rootScope, $location, $ionicPopup) {
    $http.get("https://ibmcloudant01.cloudant.com/mcon/sessionnotes").then(function(response) {
      alert(JSON.stringify(response.data));
      $scope.view_session = response.data;

    })
  })

  .controller('confspeakersCtrl', function($scope, $rootScope) {
    $scope.choice = $rootScope.choiceset;

    alert(JSON.stringify($scope.choice));

  })
  .controller('speakersCtrl', function($scope, $http, $rootScope, $ionicPopup) {

    $scope.showPopup = function() {
      $scope.data = {}
      var myPopup = $ionicPopup.show({
        template: ' Enter Speaker name<input type="text" ng-model="data.speakername">   <br> Enter Designation <input type="text" ng-model="data.designation" > ',
        title: 'Speaker Details',
        scope: $scope,
        buttons: [{
            text: 'Cancel'
          },
          {
            text: '<b>Save</b>',
            type: 'button-positive',
            onTap: function(e) {
              if (!$scope.data.speakername) {
                e.preventDefault();
              } else {
                return $scope.data;
              }
            }

          },
        ]

      });

      myPopup.then(function(res) {
        if (res) {

          var result = {
            "speakername": res.speakername,
            "designation": res.designation,
          }

          $rootScope.result1 = result;
          alert(JSON.stringify($rootScope.result1));

          console.log(JSON.stringify(result));

        }
        $http.post("https://ibmcloudant01.cloudant.com/mcon/speakers", result).then(function(response) {
          if (response.data.error == undefined) {

            console.log("Success");
            console.log(JSON.stringify(response));
          } else {

            console.log("Error:");
          }

        })
        console.log(JSON.stringify(result));
      })

    }


  })

  .controller('PlaylistCtrl', function($scope, $stateParams) {
    alert("a");
  });
