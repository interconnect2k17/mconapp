// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers'])

.run(function($ionicPlatform) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
    }
  });
})

.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

    .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'templates/menu.html',
    controller: 'AppCtrl'
  })

  .state('app.search', {
    url: '/search',
    templateUrl: 'templates/search.html',
	controller: 'MainCtrl'
  })
  .state('app.login', {
    url: '/login',
    views: {
      'menuContent': {
        templateUrl: 'templates/login.html'
      }
    }
  })


  .state('app.addEmployees', {
      url: '/addEmployees',
      views: {
        'menuContent': {
          templateUrl: 'templates/addEmployees.html',
          controller:'addConferenceCtrl'
        }
      }
  })
 .state('app.addspeakers', {
      url: '/addSpeakers',
      views: {
        'menuContent': {
          templateUrl: 'templates/speaker.html',
          controller:'addConferenceCtrl'
        }
      }
  })
	.state('app.addconference', {
      url: '/addConference',
      views: {
        'menuContent': {
          templateUrl: 'templates/addConference.html',
          controller: 'addConferenceCtrl'
        }
      }
  })
  .state('app.addagenda', {
      url: '/addAgenda',
      views: {
        'menuContent': {
          templateUrl: 'templates/addAgenda.html',
          controller: 'addConferenceCtrl'
        }
      }
  })
  .state('app.editconferences', {
          url: '/editconferences',
          views: {
            'menuContent': {
              templateUrl: 'templates/editconferences.html'
            }
          }
    })
  .state('app.activeconferences', {
        url: '/activeConferences',
        views: {
          'menuContent': {
            templateUrl: 'templates/activeConferences.html',
            controller: 'ActiveConferencesCtrl'
          }
        }
  })
  .state('app.inactiveconferences', {
        url: '/inactiveConferences',
        views: {
          'menuContent': {
            templateUrl: 'templates/inactiveConferences.html',
            controller:'inactiveConferencesCtrl'
          }
        }
  })
  .state('app.viewconferences', {
        url: '/viewConferences',
        views: {
          'menuContent': {
            templateUrl: 'templates/viewConferences.html'
          }
        }
  })
  .state('app.viewconferencesdetails', {
        url: '/viewConferenceDetails',
        views: {
          'menuContent': {
            templateUrl: 'templates/viewConferenceDetails.html',
			controller:'conferenceDashboardCtrl'
          }
        }
  })

    .state('app.addMeeting', {
        url: '/addMeeting',
        views: {
          'menuContent': {
            templateUrl: 'templates/addMeeting.html',
			controller:'addMeetingdetailsCtrl'
          }
        }
      })
.state('app.meetingdetails', {
        url: '/meetingdetails',
        views: {
          'menuContent': {
            templateUrl: 'templates/meetingdetails.html',
			controller:'meetingdetailsCtrl'
          }
        }
      })

    .state('app.admindashboard', {
      url: '/dashboard',
      views: {
        'menuContent': {
          templateUrl: 'templates/adminDashboard.html',
          controller: 'conferenceDashboardCtrl'
        }
      }
    })
    .state('app.activemeetings', {
          url: '/activeMeetings',
          views: {
            'menuContent': {
              templateUrl: 'templates/activeMeetings.html',
			   controller: 'ActivemeetingCtrl'
            }
          }
    })
    .state('app.inactivemeetings', {
          url: '/inactiveMeetings',
          views: {
            'menuContent': {
              templateUrl: 'templates/inactiveMeetings.html',
			  controller: 'InactivemeetingCtrl'
            }
          }
    })
    .state('app.viewmeetings', {
          url: '/viewMeetings',
          views: {
            'menuContent': {
              templateUrl: 'templates/viewMeetings.html'
            }
          }
    })
	 .state('app.confspeakers', {
          url: '/confspeakers',
          views: {
            'menuContent': {
              templateUrl: 'templates/confspeakers.html',
			  controller:'conferenceDashboardCtrl'
            }
          }
    })
	 .state('app.speakers', {
          url: '/speakers',
          views: {
            'menuContent': {
              templateUrl: 'templates/speakers.html',
			  controller:'speakersCtrl'
            }
          }
    })
	 .state('app.editAgenda', {
          url: '/editAgenda',
          views: {
            'menuContent': {
              templateUrl: 'templates/editAgenda.html'
            }
          }
    })
  .state('app.single', {
    url: '/playlists/:playlistId',
    views: {
      'menuContent': {
        templateUrl: 'templates/playlist.html',
        controller: 'PlaylistCtrl'
      }
    }
  })

  .state('app.userConferences', {
     url: '/userConferences',
     views: {
       'menuContent': {
         templateUrl: 'templates/userConferences.html',
		 controller:'UserConferencesCtrl'
       }
     }
 })

  .state('app.userdashboard', {
      url: '/userdashboard',
      views: {
        'menuContent': {
          templateUrl: 'templates/userdashboard.html',
          controller: 'conferenceDashboardCtrl'
        },
      }
  })
    .state('app.usersessions', {
      url: '/usersessions',
      views: {
        'menuContent': {
          templateUrl: 'templates/usersessions.html',
		  controller:'userssessionsCtrl'
        }
      }
  })

   .state('app.teamsessions', {
      url: '/teamsessions',
      views: {
        'menuContent': {
          templateUrl: 'templates/teamsessions.html',
		  controller:'teamsessionsCtrl'
        }
      }
  })
   .state('app.toursessions', {
      url: '/toursessions',
      views: {
        'menuContent': {
          templateUrl: 'templates/toursessions.html'
        }
      }
  })
  .state('app.editsessions', {
      url: '/editsessions',
      views: {
        'menuContent': {
          templateUrl: 'templates/editsessions.html'
        }
      }
  })

  .state('app.usermeetings', {
      url: '/usermeetings',
      views: {
        'menuContent': {
          templateUrl: 'templates/usermeetings.html'
        }
      }
  })
    .state('app.view_session', {
      url: '/view_session',
      views: {
        'menuContent': {
          templateUrl: 'templates/view_session.html',
		  controller:'view_sessionCtrl'
        }
      }
  })
    .state('app.add_session', {
      url: '/add_session',
      views: {
        'menuContent': {
          templateUrl: 'templates/add_session.html',
		  controller:'add_sessionCtrl'
        }
      }
  })

     .state('app.viewmeeting', {
      url: '/viewmeeting',
      views: {
        'menuContent': {
          templateUrl: 'templates/viewmeeting.html',
		  controller:'viewmeetingCtrl'
        }
      }
  })
   .state('app.assignemployee', {
      url: '/assignemployee',
      views: {
        'menuContent': {
          templateUrl: 'templates/assignemployee.html'
        }
      }
  })

   .state('app.upcomings', {
     url: '/upcomings',
     views: {
       'menuContent': {
         templateUrl: 'templates/upcomings.html',
		 controller:'UpcomingmeetingCtrl'
       }
     }
 })
  .state('app.previous', {
     url: '/previous',
     views: {
       'menuContent': {
         templateUrl: 'templates/previous.html',
		 controller:'previousmeetingCtrl'
       }
     }
 })
  .state('app.newattendee', {
      url: '/newattendee',
      views: {
        'menuContent': {
          templateUrl: 'templates/newattendee.html',
		  controller:'newattendeeCtrl'
        }
      }
  })
  .state('app.view_newattendee', {
      url: '/view_newattendee',
      views: {
        'menuContent': {
          templateUrl: 'templates/view_newattendee.html',
		  controller:'view_newattendeeCtrl'
        }
      }
  })
     .state('app.updates', {
      url: '/updates',
      views: {
        'menuContent': {
          templateUrl: 'templates/updates.html'
        }
      }
  })
   .state('app.notification1', {
      url: '/notification1',
      views: {
        'menuContent': {
          templateUrl: 'templates/notification1.html'
        }
      }
  })

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/login');
});
